import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;


//classification backpropagation
public class Main
{ 
	static double trainingRatio = 0.75; // other % will be used for validation
	static int numClasses = 3;
	static int numInputs = 13;
	static int numHiddenNodesPerLayer = 15;
	static int numHiddenLayers = 1;
	static double requiredValidationCorrectPercentage = 0.80;
	static int itersToTry = 10000;//numHiddenNodesPerLayer * 1000;
	
    static double[] mins = new double[numInputs];
    static double[] maxs = new double[numInputs];
    
    static HashMap<Matrix, Matrix> allDataSet;
	static HashMap<Matrix, Matrix> trainingSet;
	static HashMap<Matrix, Matrix> validationSet;
	
	static String inputFile = "src/data/test_input_realpart4";
    static int maxRetries = 5;
    
	public static void main(String []args)
	{
	    for (int i = 0; i < numInputs; i++)
	    {
	    	mins[i] = 9999999f;
	    }
		

		parseInput(inputFile);

	    
		//numInputs Inputs, 3 outputs, 1 hidden layer, 5 nodes
		
		NeuralNetwork network = new NeuralNetwork(numInputs,numClasses,numHiddenLayers, numHiddenNodesPerLayer);

		//I dont know why, but i keep getting into ruts, just keep trying until IT GIVES A GOOD VALUE
		int retries = 0;
		while (!train(network, itersToTry, trainingRatio, requiredValidationCorrectPercentage))
		{
			network = new NeuralNetwork(numInputs,numClasses,numHiddenLayers, numHiddenNodesPerLayer);
			retries++;
			if (retries >= maxRetries)
			{
				System.out.println("You're doomed, try different params.");
				return;
			}
		}
		
		
		
		Matrix example1 = new Matrix(1,numInputs);
		example1.setVal(0, 0, 13.72);
		example1.setVal(0, 1, 1.43);
		example1.setVal(0, 2, 2.5);
		example1.setVal(0, 3, 16.7);
		example1.setVal(0, 4, 108);
		example1.setVal(0, 5, 3.4);
		example1.setVal(0, 6, 3.67);
		example1.setVal(0, 7, 0.19);
		example1.setVal(0, 8, 2.04);
		example1.setVal(0, 9, 6.8);
		example1.setVal(0, 10, 0.89);
		example1.setVal(0, 11, 2.87);
		example1.setVal(0, 12, 1285);
		normalize(example1);
		Matrix outputMatrix1 = network.process(example1);
		outputMatrix(outputMatrix1);
		NeuralOutput output1 = argMaxIndex(outputMatrix1);
		//output specs
		System.out.println("BEST VALUE IS " + output1.confidence + " class is: " + (output1.index+1));
		
		
		Matrix example2 = new Matrix(1,numInputs);
		example2.setVal(0, 0, 12.04);
		example2.setVal(0, 1, 4.3);
		example2.setVal(0, 2, 2.38);
		example2.setVal(0, 3, 22);
		example2.setVal(0, 4, 80);
		example2.setVal(0, 5, 2.1);
		example2.setVal(0, 6, 1.75);
		example2.setVal(0, 7, 0.42);
		example2.setVal(0, 8, 1.35);
		example2.setVal(0, 9, 2.6);
		example2.setVal(0, 10, 0.79);
		example2.setVal(0, 11, 2.57);
		example2.setVal(0, 12, 580);
		normalize(example2);
		Matrix outputMatrix2 = network.process(example2);
		outputMatrix(outputMatrix2);
		NeuralOutput output2 = argMaxIndex(outputMatrix2);
		//output specs
		System.out.println("BEST VALUE IS " + output2.confidence + " class is: " + (output2.index+1));
		
		
		Matrix example3 = new Matrix(1,numInputs);
		example3.setVal(0, 0, 14.13);
		example3.setVal(0, 1, 4.1);
		example3.setVal(0, 2, 2.74);
		example3.setVal(0, 3, 24.5);
		example3.setVal(0, 4, 96);
		example3.setVal(0, 5, 2.05);
		example3.setVal(0, 6, 0.76);
		example3.setVal(0, 7, 0.56);
		example3.setVal(0, 8, 1.35);
		example3.setVal(0, 9, 9.2);
		example3.setVal(0, 10, 0.61);
		example3.setVal(0, 11, 1.6);
		example3.setVal(0, 12, 560);
		normalize(example3);
		Matrix outputMatrix3 = network.process(example3);
		outputMatrix(outputMatrix3);
		NeuralOutput output3 = argMaxIndex(outputMatrix3);
		//output specs
		System.out.println("BEST VALUE IS " + output3.confidence + " class is: " + (output3.index+1));
		
		
	}
	
	private static boolean train(NeuralNetwork network, int numIter, double trainingRatio, double validationCap)
	{
		List<Matrix> dataKeys = new ArrayList<Matrix>(allDataSet.keySet());
		Collections.shuffle(dataKeys);
		
		int index = 0;
		for (; index < dataKeys.size() * trainingRatio; index++)
		{
			trainingSet.put(dataKeys.get(index), allDataSet.get(dataKeys.get(index)));
		}
		
		for (; index < dataKeys.size(); index++)
		{
			validationSet.put(dataKeys.get(index), allDataSet.get(dataKeys.get(index)));
		}
		
		
		//outputMatrix(allDataSet.keySet().toArray(new Matrix[0])[0]);
		network.train(trainingSet, numIter);
		
		int valid = 0;
		
		for (Matrix inputMatrix : validationSet.keySet())
		{
			
			Matrix expectedMatrix = validationSet.get(inputMatrix);
			Matrix outputMatrix = network.process(inputMatrix);
			NeuralOutput expectedOutput = argMaxIndex(expectedMatrix);
			NeuralOutput actualOutput = argMaxIndex(outputMatrix);
			if (expectedOutput.index == actualOutput.index)
			{
				++valid;
			}
		}
		
		double validationCorrect = (double)valid/validationSet.keySet().size();
		System.out.println("VALIDATION SET CORRECT " + validationCorrect);
		return validationCorrect > validationCap;
		
     
	}
	
	private static void parseInput(String inputFile)
	{
		BufferedReader bufferedReader = null;
		try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = 
                new FileReader(inputFile);

            // Always wrap FileReader in BufferedReader.
            bufferedReader = 
                new BufferedReader(fileReader);

			String line;
			allDataSet = new HashMap<Matrix,Matrix>();

			trainingSet = new HashMap<Matrix,Matrix>();
			validationSet = new HashMap<Matrix,Matrix>();
            
            int numClasses = 3;
            
			while((line = bufferedReader.readLine()) != null) {
        		Matrix outputMatrix = new Matrix(1,numClasses);
        
            	String[] data = line.split(",");
            	Matrix inputMatrix = new Matrix(1,numInputs);
            	//Class 0 , 1 , 2
            	//TODO think of how to normalize this????.
            	outputMatrix.setVal(0, Integer.parseInt(data[0]) - 1, 1);
            	
            	for (int i = 1; i < numInputs+1; i++)
            	{
            		double val = Double.parseDouble(data[i]);
            		int realSlot = i - 1;
            		
            		if (val < mins[realSlot])
            		{
            			mins[realSlot] = val;
            		}
            		if (val > maxs[realSlot])
            		{
            			maxs[realSlot] = val;
            		}
            		inputMatrix.setVal(0,realSlot, val);
            	}
            	allDataSet.put(inputMatrix, outputMatrix);
			}   
			
			//normalizing
			for (Matrix matrix : allDataSet.keySet())
			{
				normalize(matrix);
			}
			
   
			// Always close files.
            bufferedReader.close();         
        }
        catch (Exception e)
		{
			System.out.println("ERROR OPEN FILE?"  + e.getMessage());
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (bufferedReader != null)
					bufferedReader.close();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	//change values to 0-1
	private static void normalize(Matrix matrix)
	{
		for (int i = 0; i < numInputs; i++)
		{
			double val = matrix.getVal(0, i);
			double percent = (val - mins[i])/(maxs[i] - mins[i]);
			matrix.setVal(0,i, percent);
		}
	}
	
	public static void outputMatrix(Matrix m)
	{
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				System.out.print(String.format(" %.5f", m.getVal(i,j)));
			}
			System.out.println();
		}
	}
	
	public static NeuralOutput argMaxIndex(Matrix m)
	{
		NeuralOutput output = new NeuralOutput();
		
		for (int i = 0; i < m.getCols(); i++)
		{
			double val = m.getVal(0, i);
			if (output.confidence < val)
			{
				output.confidence = val;
				output.index = i;
			}
		
		}
		
		return output;
	}

}
