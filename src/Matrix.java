import java.util.Random;


public class Matrix
{
	public class Size
	{
		public int rows;
		public int cols;
		public Size(int a, int b)
		{
			rows = a; cols = b;
		}
		
		@Override
		public String toString()
		{
			return "Rows: " + rows + " Cols: " + cols;
		}
	}
	
	double [][]matrix;
	int rows;
	int cols;
	private static Random rand = new Random();
	
	public Matrix(int rows, int cols)
	{
		this.rows = rows;
		this.cols = cols;
		matrix = new double[rows][cols];
	}
	
	public Size getSize()
	{
		return new Size(rows, cols);
	}
	
	public Matrix multiply(Matrix m2)
	{
		if (this.getCols() != m2.getRows())
		{
			throw new IllegalArgumentException("ILLEGAL MULTIPLY DIMENSIONS THIS " + this.getSize().toString() + " M2: " + m2.getSize().toString());
		}
		
		Matrix newMatrix = new Matrix(rows, m2.getCols());
		
		for (int row = 0; row < rows; row++)
		{
			
			for (int col = 0; col < m2.getCols(); col++)
			{
				double total = 0;
				for (int i = 0; i < cols; i++)
				{
					total += this.getVal(row, i) * m2.getVal(i, col);
				}
				newMatrix.setVal(row, col, total);
			}
		}
		return newMatrix;
	}
	
	public Matrix add(Matrix m)
	{
		if (m.getRows() != this.getRows() || m.getCols() != this.getCols())
		{
			throw new IllegalArgumentException("ILLEGAL ADD DIMENSIONS THIS " + this.getSize().toString() + " M2: " + m.getSize().toString());
		}
		
		Matrix newMatrix = new Matrix(m.getRows(), m.getCols());
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				newMatrix.setVal(i, j, getVal(i,j) + m.getVal(i, j));
			}
		}
		return newMatrix;
	}
	

	public Matrix subtract(Matrix m)
	{
		if (m.getRows() != this.getRows() || m.getCols() != this.getCols())
		{
			throw new IllegalArgumentException("ILLEGAL SUB DIMENSIONS THIS " + this.getSize().toString() + " M2: " + m.getSize().toString());
		}
		
		Matrix newMatrix = new Matrix(m.getRows(), m.getCols());
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				newMatrix.setVal(i, j, getVal(i,j) - m.getVal(i, j));
			}
		}
		return newMatrix;
	}
	
	public Matrix scalarMultiply(double val)
	{
		Matrix newMatrix = new Matrix(getRows(), getCols());
		for (int i = 0; i < getRows(); i++)
		{
			for (int j = 0; j < getCols(); j++)
			{
				newMatrix.setVal(i, j, getVal(i,j) * val);
			}
		}
		return newMatrix;
	}
	
	public Matrix flatMultiply(Matrix m)
	{
		if (m.getRows() != this.getRows() || m.getCols() != this.getCols())
		{
			throw new IllegalArgumentException("ILLEGAL FLAT MULT DIMENSIONS THIS " + this.getSize().toString() + " M2: " + m.getSize().toString());
		}
		
		Matrix newMatrix = new Matrix(m.getRows(), m.getCols());
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				newMatrix.setVal(i, j, getVal(i,j) * m.getVal(i, j));
			}
		}
		return newMatrix;
	}
	
	public int getCols()
	{
		return cols;
	}
	
	public int getRows()
	{
		return rows;
	}
	
	public double getVal(int row, int col)
	{
		return matrix[row][col];
	}
	
	public void setVal(int row, int col, double val)
	{
		matrix[row][col] = val;
	}
	
	public Matrix transpose()
	{
		Matrix newMatrix = new Matrix(getCols(), getRows());
		for (int i = 0; i < getRows(); i++)
		{
			for (int j = 0; j < getCols(); j++)
			{
				newMatrix.setVal(j,i, getVal(i,j));
			}
		}
		return newMatrix;
	}

	//Setup all values 0 to 0.2
	public void initRandomValues()
	{
		for (int row = 0; row < rows; row++)
		{
			for (int col = 0; col < cols; col++)
			{
				matrix[row][col] = rand.nextDouble() * 0.5;
			}
		}
		
	}
	

	
}
