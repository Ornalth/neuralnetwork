
public interface IActivationFunction
{
	double activation(double x);
	double activationDerivative(double x);
}
