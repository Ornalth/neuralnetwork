import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//Maybe do introduce bias and momentum later.

//momentum
//http://stackoverflow.com/questions/9369198/neural-network-back-propagation-algorithm-gets-stuck-on-xor-training-pattern
public class NeuralNetwork
{
	List<Matrix> weightMatrices;

	int numInput;
	int numOutput;

	int numNodesPerHiddenLayer;
	int numHiddenLayers;

	double learningRate = 0.1;
	IActivationFunction activationFcn;
	
	//Default sigmoid
	public NeuralNetwork(int numInput, int numOutput, int numHiddenLayers,
			int numNodesPerHiddenLayer)
	{
		this(numInput, numOutput, numHiddenLayers, numNodesPerHiddenLayer, new IActivationFunction()
		{
			@Override
			public double activationDerivative(double x)
			{
				double sig = activation(x);
				return sig * (1 - sig);
			}
			
			@Override
			public double activation(double x)
			{
				return 1 / (1 + Math.exp(-x));
			}
		});
	}
	
	public NeuralNetwork(int numInput, int numOutput, int numHiddenLayers,
			int numNodesPerHiddenLayer, IActivationFunction activationFcn)
	{
		this.numInput = numInput;
		this.numOutput = numOutput;
		this.numHiddenLayers = numHiddenLayers;
		this.numNodesPerHiddenLayer = numNodesPerHiddenLayer;
		weightMatrices = new ArrayList<Matrix>();

		int curNumberOfNodes = numInput;
		int nextNumberOfNodes = numNodesPerHiddenLayer;

		for (int i = 0; i < numHiddenLayers; i++)
		{
			Matrix weightMatrix = new Matrix(curNumberOfNodes,
					nextNumberOfNodes);
			weightMatrix.initRandomValues();
			weightMatrices.add(weightMatrix);
			// outputMatrix(weightMatrix);
			curNumberOfNodes = nextNumberOfNodes;
			// System.out.println(weightMatrix.getSize().toString());
		}

		nextNumberOfNodes = numOutput;
		Matrix finalWeightMatrix = new Matrix(curNumberOfNodes,
				nextNumberOfNodes);
		finalWeightMatrix.initRandomValues();
		// outputMatrix(finalWeightMatrix);
		// System.out.println(finalWeightMatrix.getSize().toString());
		weightMatrices.add(finalWeightMatrix);
		
		this.activationFcn = activationFcn;
	}



	public Matrix activationFcn(Matrix m)
	{
		Matrix newMatrix = new Matrix(m.getRows(), m.getCols());
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				newMatrix.setVal(i, j, activationFcn.activation(m.getVal(i, j)));
			}
		}
		return newMatrix;
	}

	public Matrix derivativeActivationFcn(Matrix m)
	{
		Matrix newMatrix = new Matrix(m.getRows(), m.getCols());
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				newMatrix.setVal(i, j, activationFcn.activationDerivative(m.getVal(i, j)));
			}
		}
		return newMatrix;
	}

	public Matrix process(Matrix inputMatrix)
	{
		// Matrix outputMatrix = new Matrix(1, numOutput);
		Matrix outputMatrix = inputMatrix;
		for (Matrix matrix : weightMatrices)
		{
			outputMatrix = activationFcn(outputMatrix.multiply(matrix));
		}
		return outputMatrix;
	}

	public void train(Map<Matrix, Matrix> trainingSet, int maxIter)
	{
		//double minError = 0.04 * trainingSet.size();
		//System.out.println("WANT " + minError);
		
		double totalError = trainingSet.size();

		
		double bestError = trainingSet.size();
		List<Matrix> bestWeightsMatrix = new ArrayList<Matrix>(weightMatrices);
		
		List<Matrix> deltaMatrices = new ArrayList<Matrix>();

		for (int i = 0; i < weightMatrices.size(); i++)
		{
			deltaMatrices.add(new Matrix(1, 1));
		}

		while (maxIter > 0)
		{
			totalError = 0;
			for (Matrix inputMatrix : trainingSet.keySet())
			{
				// System.out.println("CURRENT ITER " + numIter);
				// http://ufldl.stanford.edu/wiki/index.php/Backpropagation_Algorithm

				List<Matrix> inputMatrices = new ArrayList<Matrix>();
				List<Matrix> outputMatrices = new ArrayList<Matrix>();

				Matrix currentInputMatrix = inputMatrix;
				Matrix currentOutputMatrix = null;

				// Labels
				Matrix expectedMatrix = trainingSet.get(inputMatrix);

				// Forward propagation & store values for gradient descent
				for (Matrix weightMatrix : weightMatrices)
				{
					inputMatrices.add(currentInputMatrix);
					Matrix weightXInput = currentInputMatrix
							.multiply(weightMatrix);
					currentOutputMatrix = activationFcn(weightXInput);
					outputMatrices.add(currentOutputMatrix);
					currentInputMatrix = currentOutputMatrix;
				}

				totalError += getError(currentOutputMatrix, expectedMatrix);
				// double total = sum(derivativeActivationFcn(outputMatrix));
				//outputMatrix(currentOutputMatrix);
				Matrix errorOutput = expectedMatrix
						.subtract(currentOutputMatrix);
				//outputMatrix(currentOutputMatrix);
				//outputMatrix(expectedMatrix);

				// OutputDelta
				Matrix currentDelta = errorOutput
						.flatMultiply(derivativeActivationFcn(currentOutputMatrix));
				deltaMatrices.set(weightMatrices.size() - 1, currentDelta);

				// Skip output layer
				//Calculate deltas
				for (int i = weightMatrices.size() - 2; i >= 0; i--)
				{
					Matrix theWeightMatrix = weightMatrices.get(i + 1);
					Matrix theZMatrix = outputMatrices.get(i);
					// Matrix theAMatrix = inputMatrices.get(i);
					currentDelta = theWeightMatrix
							.multiply(currentDelta.transpose()).transpose()
							.flatMultiply(derivativeActivationFcn(theZMatrix));
					deltaMatrices.set(i, currentDelta);
				}

				//Update weights
				for (int i = 0; i < weightMatrices.size(); i++)
				{
					Matrix delta = deltaMatrices.get(i);
					Matrix theAMatrix = inputMatrices.get(i);
					Matrix oldWeightsMatrix = weightMatrices.get(i);

					Matrix changesMatrix = delta.transpose()
							.multiply(theAMatrix).transpose()
							.scalarMultiply(learningRate);

					//outputMatrix(changesMatrix);
					
					Matrix newWeightMatrix = oldWeightsMatrix
							.add(changesMatrix);
					//if (maxIter % 100 == 0)
					//{
					//	outputMatrix(newWeightMatrix);
					//}
					weightMatrices.set(i, newWeightMatrix);
				}
			}

			//System.out.println("ERROR IS " + totalError);
			if (totalError < bestError)
			{
				bestWeightsMatrix = new ArrayList<Matrix>(weightMatrices);
				bestError = totalError;
			}
			
			maxIter--;
		}
		
		weightMatrices = bestWeightsMatrix;

	}

	public static void outputMatrix(Matrix m)
	{
		for (int i = 0; i < m.getRows(); i++)
		{
			for (int j = 0; j < m.getCols(); j++)
			{
				System.out.print(String.format(" %.5f", m.getVal(i, j)));
			}
			System.out.println();
		}
	}

	private double getError(Matrix output, Matrix expected)
	{
		double error = 0;
		for (int i = 0; i < output.getCols(); i++)
		{
			double diff = output.getVal(0, i) - expected.getVal(0, i);
			error += (diff * diff);
		}
		return error / 2.0;
	}

}
